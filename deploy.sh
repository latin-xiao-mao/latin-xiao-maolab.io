#!/usr/bin/env sh

# 推送到github私库
git add .
git commit -m "update site :construction: :sparkles:"
git push origin master -f --tags


# 推送到 coding
git remote add coding git@e.coding.net:latin-xiao-mao/latin-xiao-mao.git
git push coding master -f --tags


# 推送到 gitlab
git remote add gitlab git@gitlab.com:latin-xiao-mao/latin-xiao-mao.gitlab.io.git
git push gitlab master -f --tags
