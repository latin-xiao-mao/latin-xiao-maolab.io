---
title: Git-commit-中添加表情
cover: 'https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/cover/57.jpg'
categories: 编程
tags:
  - Git
abbrlink: 6566c4e5
date: 2020-06-23 09:23:43
---

# git commit 中使用表情
我们经常可以在github上看到国外大佬的commit信息中有很多可爱的表情，这是怎么做到的呢？
ok，可以这样使用哦：`git commit -m '提交信息 :emoji:'`，示例：`git commit -m '增加新功能 :sparkles:'`

# 有哪些表情呢
来了，请参照这个链接：
https://gitmoji.carloscuesta.me/

# demo
<img src="https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/blog-content/git-commit-emoji/1.png"/>
<img src="https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/blog-content/git-commit-emoji/2.png"/>
