---
title: 回忆
cover: 'https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/cover/69.jpg'
categories:
  - 生活
tags:
  - 图片
password: 123456
message: 联系latinxiaomao@gmail.com获取密码
abbrlink: c4ad2d2d
date: 2020-06-26 00:06:23
---



# 关于相册

相册呢，我托管在另外一个站点上，欢迎访问

请访问 <a href="https://latin-xiao-mao.gitee.io/photography">我的相册</a> 站点浏览

<img src="https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/cover/85.jpg"/>
