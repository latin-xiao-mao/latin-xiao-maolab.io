---
title: 音乐欣赏
date: 2020-06-23 23:00:23
type: "music"
layout: music
updated: 2020-06-23 23:03:13
comments: true
description: 我喜欢的音乐
keywords: music
top_img: https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/cover/60.jpg
mathjax:
katex:
aside: true
aplayer: true
highlight_shrink:
---


{% meting "523845661" "netease" "playlist" "theme:#FF4081" "mode:circulation" "mutex:true" "listmaxheight:340px" "preload:auto" %}

{% meting "2501380380" "netease" "playlist" "theme:#FF4081" "mode:circulation" "mutex:true" "listmaxheight:340px" "preload:auto" %}
