---
title: 关于我
date: 2020-05-18 06:23:46
layout: about
type: "about"
top_img: https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/cover/12.jpg
---

<iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=500 height=86 src="//music.163.com/outchain/player?type=2&id=421091290&auto=1&height=66" muted="muted"></iframe>

# 关于我

<div style="background-image: url(https://cdn.jsdelivr.net/gh/latin-xiao-mao/img/gallery/50.jpg); font-family: cursive; color: aliceblue; height: 500px;">
我自认为我是属于90后中年龄偏大的一批人了，但是技术却很菜。我有时候的想法确实跟大部分人不同，说是不同吧，其实是几乎没人那么去想（从这一点也可以看出我很笨啦）；我虽然属于北方人，但是对于南北方的感受并没有多少的差异。</div>
